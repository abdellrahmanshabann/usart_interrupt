#include <stm32f10x.h>
#include "Types.h"
void SystemInit()
{
	
}
	volatile uint8 intt_flag=0;
	volatile uint8 data;
void Uart1_Init(void)
{
	RCC 	 	->	APB2ENR		|=	(1<<2) | (1<<0) | (1<<14);	//RCC_USART1 	ENABLE -- RCC_USART3 	ENABLE -- UART1 Enable
	GPIOA 	-> 	CRH				 =	0X000004B0;				//Configure TX , RX pins
	//AFIO 		->	MAPR			 = 	0X00000000;				//AFIO_MAPR			not mapping
	
	USART1	->	CR1 			|=	(1<<13);					//UART1 ENABLE
	USART1 	->	CR1 			|=	(1<<3) | (1<<2);	//UART1_TRANSMIT ENABLE -- UART1_RECIEVER ENABLE
	USART1 	->	CR1 			|=	 (1<<5);	//UART1_TCIE ENABLE -- UART1_TXEIE ENABLE
	USART1 	->	CR1 			&=~	(1<<12);					//8-bit word lenght
	USART1 	->	CR2 			&=~	(1<<13);					//1 stop bit
	USART1 	->	CR2 			&=~	(1<<12);					//1 stops bit
	USART1  ->	BRR				 =	8000000/9600;			//BAUD RATE
}

void Uart1_TX(uint8 Data)
{
		USART1 -> DR = Data;											//Send data
		//while((USART1 -> CR1 & (1<<6)) == 1);			//if USART1_CR1_TCIE is Enable										
}

uint8 Uart1_RX()
{
	if(intt_flag==1)
	{
		if(((USART1 -> SR)&(1<<5)) != 1)				//waiting while recieve flag
		{
			return (USART1->DR);									//return Data
			intt_flag=0;
		}
	}
}

void USART1_IRQHandler()
{
	intt_flag=1;					//receiver interrupt
	//if(UART1 -> SR & (1<<5))
		data=(USART1->DR&0X1ff);
}

int main()
{
	RCC 		-> 	APB2ENR 	|= 	(1<<4);			//Enable portC
	GPIOC		-> 	CRH				 =	0X00300000;	///Configure portc pin13
	//NVIC->ISPR[1]=USART1_IRQn;
	NVIC->ISER[((uint32)(37)>>5)] = (1<<((uint32) (37)&0x1f));
	//uint8 Data = 'a' , Recieve=0;
	Uart1_Init();
	while(1)
	{
		if(intt_flag!=0)
		{
			Uart1_TX(data);
			GPIOC -> ODR ^= (1<<13);					//Toggle GPIOC_PIN13 OUTPUT (LED)
			for(double i=0;i<100000;i++);
			intt_flag=0;
		}
	}
}

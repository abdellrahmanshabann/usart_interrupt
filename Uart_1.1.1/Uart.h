#include "Types.h"

#define BAUD 9600
#define CLOCK 8000000
typedef enum{
	US_SR_PE=0,
	US_SR_FE,
	US_SR_NE,
	US_SR_ORE,
	US_SR_IDLE,
	US_SR_RXNE,
	US_SR_TC,
	US_SR_TXE,
	US_SR_LBD,
	US_SR_CTS,
}Usart_SR;

typedef enum{
	US_CR1_SBK=0,
	US_CR1_RWU,
	US_CR1_RE,
	US_CR1_TE,
	US_CR1_IDLEIE,
	US_CR1_RXNEIE,
	US_CR1_TCIE,
	US_CR1_TXEIE,
	US_CR1_PEIE,
	US_CR1_PS,
	US_CR1_PCE,
	US_CR1_WAKE,
	US_CR1_M,
	US_CR1_UE,
	
}Usart_CR1;
/*LINEN STOP[1:0] CLK
EN CPOL CPHA LBCL Res. LBDIE LBDL Res. ADD[3:0]*/
typedef enum{
	US_CR2_ADD0=0,
	US_CR2_ADD1,
	US_CR2_ADD2,
	US_CR2_ADD3,
	US_CR2_RES,
	US_CR2_LBDL,
	US_CR2_LBDIE,
	US_CR2_RRES,
	US_CR2_LBCL,
	US_CR2_CPHA,
	US_CR2_CPOL,
	US_CR2_EN,
	US_CR2_CLKEN,
	US_CR2_STOP1,
	US_CR2_STOP2,
	US_CR2_LINEN,
}Usart_CR2;



void Uart1_Init(void);
void Uart1_TX(uint8 Data);
uint8 Uart1_RX(void);
void Uart1_TX_String(uint8 *Data);
//uint8 Uart1_RX_String();

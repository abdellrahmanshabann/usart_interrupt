#include <stm32f10x.h>
#include "Uart.h"
#include "Rcc.h"

extern volatile uint8 data;

void Uart1_Init(void)
{
	RCC 	 	->	APB2ENR		|=	(1<<RC_APB2ENR_IOPAEN) | (1<<RC_APB2ENR_AFIOEN) | (1<<RC_APB2ENR_USART1EN);
	GPIOA 	-> 	CRH				 =	0X000004B0;				//Configure TX , RX pins
	
	
	USART1	->	CR1 			|=	(1<<US_CR1_UE);					//UART1 ENABLE
	USART1 	->	CR1 			|=	(1<<US_CR1_TE) | (1<<US_CR1_RE);	//UART1_TRANSMIT ENABLE -- UART1_RECIEVER ENABLE
	USART1 	->	CR1 			|=	 (1<<US_CR1_RXNEIE);	//UART1_TCIE ENABLE -- UART1_TXEIE ENABLE
	USART1 	->	CR1 			&=~	(1<<US_CR1_M);					//8-bit word lenght
	USART1 	->	CR2 			&=~	(1<<US_CR2_STOP2);					//1 stop bit
	USART1 	->	CR2 			&=~	(1<<US_CR2_STOP1);					//1 stops bit
	USART1  ->	BRR				 =	CLOCK/BAUD;			//BAUD RATE
}

void Uart1_TX(uint8 Data)
{
		USART1 -> DR = Data;											//Send data
		//while((USART1 -> CR1 & (1<<6)) == 1);		//if USART1_CR1_TCIE is Enable										
}

void Uart1_TX_String(uint8 *Data)
{
	for(int i=0;Data[i]!='\0';i++)
	{
		Uart1_TX(Data[i]);												//Send data
		for(int i=0;i<100000;i++);								//dealy
	}		
}

uint8 Uart1_RX(void)
{
	while(!((USART1 -> SR)&(1<<5)));				//waiting while recieve flag
			
	return (USART1->DR);												//return Data
}


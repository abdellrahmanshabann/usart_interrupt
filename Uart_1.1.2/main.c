#include <stm32f10x.h>
#include <String.h>
#include "Types.h"
#include "Rcc.h"
#include "Uart.h"
#define IRQn 37
void SystemInit()
{
	
}

uint8*Rec , intt_flag , data[10];

uint8_t Rx_data;
#define NUM 10
volatile uint8 i ,j;
volatile uint8 name[NUM+1] = {'\0'};
void USART1_IRQHandler()
{
		if ((USART1->SR & US_SR_RXNE) != (uint16)0x01)           
		{          
			i= USART1->DR&0x01FF;
		  intt_flag=1;

			if(name[j] == '\r')
			{
				GPIOC ->ODR|= (1<<13);
				for(int s=0;s<11;s++)
				{
					name[s]='\0';

				} 
		
				j=0;

			}
			else
			{
				name[j++] = i;
			}

		}   
}

int main()
{
	RCC 		-> 	APB2ENR 	|= 	(1<<RC_APB2ENR_IOPCEN);			//Enable portC
	GPIOC		-> 	CRH				 =	0X00300000;									//Configure portc pin13
	RCC 		-> 	APB2ENR 	|= 	(1<<RC_APB2ENR_IOPBEN);			//Enable portC
	GPIOB		-> 	CRH				 =	0X30330000;									//Configure portb
	
	NVIC->ISER[((uint32)(IRQn)>>5)] = (1<<((uint32) (IRQn)&0x1f));
	
	Uart1_Init();
	
	while(1)
	{
		if(intt_flag==1){
			
			//Uart1_TX_String(name);
			if(name[0]=='A'&&name[1]=='T')
			{
				GPIOB ->ODR |= (1<<GREEN);
				GPIOB ->ODR &=~ (1<<RED);
				Uart1_TX_String("OK");
				name[j]=0;
				
			}
			else{
				GPIOB ->ODR |= (1<<RED);
			}
				intt_flag=0;
		}
		
		
	}
}



typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;
typedef unsigned long long uint64;

#define RED_ON					GPIOB ->ODR |= (1<<12)
#define RED_TOGGLE			GPIOB ->ODR ^= (1<<12)
#define RED_OFF					GPIOB ->ODR &=~ (1<<12)
#define GREEN_ON				GPIOB ->ODR |= (1<<13)
#define GREEN_TOGGLE		GPIOB ->ODR ^= (1<<13)
#define GREEN_OFF				GPIOB ->ODR &=~ (1<<13)

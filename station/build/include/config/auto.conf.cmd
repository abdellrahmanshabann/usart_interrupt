deps_config := \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/app_trace/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/aws_iot/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/bt/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/driver/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/esp32/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/esp_adc_cal/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/esp_event/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/esp_http_client/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/esp_http_server/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/ethernet/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/fatfs/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/freemodbus/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/freertos/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/heap/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/libsodium/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/log/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/lwip/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/mbedtls/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/mdns/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/mqtt/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/nvs_flash/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/openssl/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/pthread/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/spi_flash/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/spiffs/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/tcpip_adapter/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/vfs/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/wear_levelling/Kconfig \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/bootloader/Kconfig.projbuild \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/esptool_py/Kconfig.projbuild \
	/home/AbdellrahmanShaaban/esp/esp-idf/examples/wifi/getting_started/station/main/Kconfig.projbuild \
	/home/AbdellrahmanShaaban/esp/esp-idf/components/partition_table/Kconfig.projbuild \
	/home/AbdellrahmanShaaban/esp/esp-idf/Kconfig

include/config/auto.conf: \
	$(deps_config)

ifneq "$(IDF_CMAKE)" "n"
include/config/auto.conf: FORCE
endif

$(deps_config): ;
